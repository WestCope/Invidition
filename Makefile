git_watched_files := $(shell git ls-files)
extension_files := $(shell find extension -type f -not -name '*.map')
extension_sourcemap_files := $(shell find extension -type f)

.PHONY: build watch clean package package-sm

all: build package

extension.zip: $(extension_files)
	cd extension && \
	find . -type f -not -name '*.map' | xargs zip ../extension.zip

extsm.zip: $(extension_sourcemap_files)
	cd extension && \
	find . -type f | xargs zip ../extsm.zip

sources.zip: $(git_watched_files)
	git ls-files | xargs zip sources.zip

package: extension.zip sources.zip

package-sm: build extsm.zip

build:
	npm run build

watch:
	npm run build:watch

clean:
	rm -r extension/dist/*;
	rm -r extension.zip sources.zip
