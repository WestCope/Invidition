import {isEnabled} from "./helpers";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";

/**
 * Toggle icon, depending on the state of the extension
 * @returns {Promise<void>}
 */
export const toggleIcon = async () => {
    await browser.browserAction.setIcon({path: `/assets/img/logo${await isEnabled() ? "" : "-disabled"}.svg`});
};
