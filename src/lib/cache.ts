// @ts-ignore
import {browser} from "webextension-polyfill-ts";

interface ICacheObject {
    data: any;
    timestamp: number;
}

/**
 * Clear the cache
 */
export const clear = async () => {
    try {
        return await browser.storage.local.remove("cache");
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get a value from the cache
 * @param name
 */
export const get = async (name: string) => {
    try {
        const loaded = await load();
        return loaded ? loaded[name] : {};
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Load the whole cache
 */
export const load = async () => {
    try {
        const cache = (await browser.storage.local.get("cache")).cache;
        return cache !== undefined ? cache : {};
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Save a value to the cache
 * @param name
 * @param data
 */
export const save = async (name: string, data: any) => {
    try {
        const cache = await load();

        cache[name] = {
            data,
            timestamp: Date.now(),
        };

        await browser.storage.local.set({cache});
    } catch (e) {
        console.error(e.message);
    }
};

/**
 *  Validate a cache object
 * @param object
 * @param ttl       Time To Live in seconds.
 */
export const validate = async (object: ICacheObject, ttl: number) => {
    try {
        return Date.now() <= object.timestamp + ttl * 1000;
    } catch (e) {
        console.error(e.message);
    }
};
